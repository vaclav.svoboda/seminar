
a) Jaké jsou tři vlastnosti algoritmu a co znamenají? (5b)
Rezultativnost - má výsledek
Konečnost - Někdy skončí
Determinovanost - každý krok algoritmu musí být přesně a jednoznačně definován

b) Jaké znáte pogramovací jazyky? Je c# kompilovaný nebo interpretovaný programovací jazyk? Rozveďte. (5b)
c#, java, python, c++ C# je kompilovaný, ale používá interpret.

c) Popište dva datové typy, do kterých můžeme ukládat soubor dat stejného datového typu, a popište, v čem se liší? (5b)
Int (rozsah -2,147,483,648 to 2,147,483,647)
ulong (rozsah 	0 to 18,446,744,073,709,551,615)

d) Co je to rekurze? Jaké znáte druhy rekurzí? Popište je. (5b)
Je to způsob, kterým vyvolám funkci, než dokončím předchozí vyvolání.
Nepřímá - v programu A se odkazuju na program B
přímá - volá sám sebe
 
e) Jak jsou realizovány řetězce v c#? Jak je v c# realizován znak? (5b)
string, realizován jako char

f) Co je to implicitní konverze? Uveďte příklad implicitní konverze. Vysvětlete, kdy může nastat při konverzi Overflow exception. (5b)
Jedná se o automatické přetypování, např když sčítám int a ulong kvůli desetiným číslům. Operant s nižší prioritou jde na operant s vyšší. Může nastat, když se číslo nevejde do datováho typu, na který ho převádím.

g) Jak se jmenuje třída, která slouží pro čtení ze souboru? Kde se soubor nachází (relativní cesta)? (5b)(nevím, zda-li jsem pochopil otázku zcela správně)
StreamReader, soubor se nachází v debugu.

h) Jak definujeme funkci s návratovou hodnotou a parametrem a jak ji zavoláme? (5b)
static void parametr(int x)    
{
nějaká funkce 
}

zpětné zavolání
parametr(x)

i) Jaké znáte řadící algoritmy? (5b)
bubble sort, shell sort, bucket sort
