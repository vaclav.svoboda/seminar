﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp6
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        string zadane;
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            zadane = textBox1.Text;
            char[] x = new char[zadane.Length];
            for (int i = 0; i < zadane.Length; i++)
            {
                x[i] = zadane[i];
            }
            BubbleSort(x);
            textBox1.Text = String.Join(", ", x);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text = String.Empty;
            Application.Restart();
        }


    }
    static void BubbleSort(char[] array)
    {
        char temp;
        for (int i = 0; i <= array.Length - 2; i++)
        {
            for (int j = 0; j <= array.Length - 2; j++)
            {
                if (array[j] > array[j + 1])
                {
                    temp = array[j + 1];
                    array[j + 1] = array[j];
                    array[j] = temp;
                }
            }
        }
    }
}
