﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test01
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Zadej délku pole");
            
            int i;
            i = int.Parse(Console.ReadLine());
            int[] pole = new int[i];
            Random rnd = new Random();
            for (int o = 0; o < i; o++) 
            {
                pole[o] = rnd.Next(0, 30);

            }
            printArray (pole);

            List<int> suda = new List<int> ();
            List<int> liche = new List<int>();

            foreach(int item in pole)
            {
                if (jeLiche(item))
                {
                    liche.Add(item);
                }
                else
                {
                    suda.Add(item);
                }
            }


            
            Console.WriteLine("Pocet sudych cisel je " + suda.Count);
            Console.WriteLine("Pocet lichých cisel je " + liche.Count);

            printArray(vypis(pole));

            Console.ReadKey();            
            
            

        }
        static void printArray(int[] array)
        {
            foreach (var item in array)
            {
                Console.Write(item + " ");
            }
            Console.WriteLine();
        }
        static bool jeLiche(int x)
        {
            if (x%2 ==0)
            {
                return false;
            }
            return true;
        }

        static int [] vypis(int[] array)
        {
            int[] vypis = new int[array.Length];
            for (int o = 0; o < array.Length; o++)
            {
                vypis[array.Length - o - 1] = array[o];

            }
            return vypis;



        }
    }
}
