﻿using System;
public class Fibonacci
{
    public static void Main(string[] args)
    {
        int n1 = 0, n2 = 1, n3, i, number;

        Console.Write("vypíšu ti prvky Fibonacciho posloupnosti ");
        Console.WriteLine();
        Console.Write("zadej počet čísel ");

        number = int.Parse(Console.ReadLine());

        Console.Write("0 ");

        for (i = 1; i < number; ++i)
        {
            n3 = n1 + n2;
            Console.Write(n3 + " ");
            n1 = n2;
            n2 = n3;
        }
        Console.WriteLine();
        Console.WriteLine("------------------------------------------------------------");
        Console.WriteLine("Stisknutím libovolné klávesy mě ukončíš");
        Console.ReadKey();
    }
}