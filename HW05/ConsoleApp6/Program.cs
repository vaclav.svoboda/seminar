﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
   
    class Program
    {
  
        static void Main(string[] args)
        {
            Console.WriteLine("Zašifrování v ASCII - zadej číslo: ");
            int i = Convert.ToInt32(Console.ReadLine());

            Rewrite(i);
            Console.ReadKey();
        }
        static void Rewrite(int i)
        {
            StreamReader file = new StreamReader("sifra.txt");
            StringBuilder sb = new StringBuilder();

            string line = file.ReadLine();

            while (line != null)
            {
                StringBuilder sb1 = new StringBuilder();
                for (int o = 0; o < line.Length; o++)
                {
                    sb1.Append((char)((int)line[o] + o));
                }

                sb.AppendLine(sb1.ToString());
               
                line = file.ReadLine();
            }
            StreamWriter write = new StreamWriter("new_sifra.txt");
            write.Write(sb);
            file.Close();
            write.Close();
        }
    }
}
