﻿using System;
public class Fibonnaci
{
    public static
        int x
        (int n1)
    {

        if (n1 <= 2)
            return 1;
        else
            return x(n1 - 1) + x(n1 - 2);
    }

    public static void Main()
    {
        int y;

        Console.WriteLine("Ahoj, vypočítám ti libovolný prvek Fibonnaciho posloupnosti");
        Console.WriteLine();
        Console.WriteLine("Zadej mi číslo ");
        y = Convert.ToInt32(Console.ReadLine());

        Console.WriteLine();
        Console.WriteLine("Člen číslo {0} je roven hodnotě {1} ", y, x(y));
        Console.Write("-------------------------------------------------------------------------------");
        Console.ReadKey();
    }
}