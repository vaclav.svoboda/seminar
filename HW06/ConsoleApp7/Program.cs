﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp7
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();
            int[] ar = new int[30];
            for (int i = 0; i < ar.Length; i++)
            {
                ar[i] = rnd.Next(0, 100);
            }
            PrintArray(ar);
            Console.WriteLine();

            BubbleSort(ar);
            PrintArray(ar);
            Console.ReadKey();
        }


        static void BubbleSort(int[] array)
        {
            int temp;
            for (int i = 0; i <= array.Length - 2; i++)
            {
                for (int o = 0; o <= array.Length - 2; o++)
                {
                    if (array[o] > array[o + 1])
                    {
                        temp = array[o + 1];
                        array[o + 1] = array[o];
                        array[o] = temp;
                    }
                }
            }
        }
        static void PrintArray(int[] array)
        {
            foreach (var item in array)
            {
                Console.Write(item + " ");
            }
        }
    }
}
