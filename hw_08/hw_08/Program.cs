﻿using System;

namespace hw_08
{
    class Program
    {
        static void Main(string[] args)
        {
            long c1, c2, hcf, lcm;
            Console.WriteLine(" Najdu ti Největší společný dělitel a nejmenší společný násobek :");
            Console.WriteLine(".................................................................");

            Console.Write(" Hoď mi sem první číslo : ");
            c1 = Convert.ToInt64(Console.ReadLine());
            Console.Write(" Teď to druhý : ");
            c2 = Convert.ToInt64(Console.ReadLine());

            hcf = gcd(c1, c2);
            lcm = (c1 * c2) / hcf;




            Console.WriteLine(" Největší společný dělitel {0} a {1} je {2} ", c1, c2, hcf);
            Console.WriteLine(" nejmenší společný násobek {0} a {1} je {2}\n", c1, c2, lcm);


        }

        static long gcd(long n1, long n2)
        {
            if (n2 == 0)
            {
                return n1;
            }
            else
            {
                return gcd(n2, n1 % n2);
            }
        }
    }
}
